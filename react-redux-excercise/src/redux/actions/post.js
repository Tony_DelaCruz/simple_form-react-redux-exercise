import { post } from '../constants/post'
import { actionCreator } from './utils'

const fetchSinglePost = payload => actionCreator(post.GET, payload)
const fetchPostSuccess = payload => actionCreator(post.SUCCESS, payload)
const postThrowError = payload => actionCreator(post.ERROR, payload)

const fetchPostComments = payload => actionCreator(post.GET_POST_COMMENTS, payload)
const fetchPostCommentSuccess = payload => actionCreator(post.GET_POST_COMMENTS_SUCCESS, payload)
const fetchPostCommentError = payload => actionCreator(post.GET_POST_COMMENTS_ERROR, payload)

export const postActions = {
    fetchSinglePost,
    fetchPostSuccess,
    postThrowError,
    fetchPostComments,
    fetchPostCommentSuccess,
    fetchPostCommentError
}