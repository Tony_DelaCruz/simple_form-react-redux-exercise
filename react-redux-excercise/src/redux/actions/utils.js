export const actionCreator = (action, payload = null) => {
    return {
        type: action,
        payload
    }
} 
