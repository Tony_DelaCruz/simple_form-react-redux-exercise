import { posts } from '../constants/posts'
import { actionCreator } from './utils'

const addPost = payload => actionCreator(posts.ADD, payload)
const fetchPosts = () => actionCreator(posts.GET)
const fetchPostsSuccess = payload => actionCreator(posts.SUCCESS, payload)
const postsThrowError = payload => actionCreator(posts.ERROR, payload)
const submitForm = payload => actionCreator(posts.SUBMIT, payload)
const submitFormError = payload => actionCreator(posts.SUBMIT_ERROR, payload)


export const postsActions = {
    addPost,
    fetchPosts,
    fetchPostsSuccess,
    postsThrowError,
    submitForm,
    submitFormError
}
