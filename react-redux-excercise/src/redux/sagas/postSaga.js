import { put, call, all, takeLatest, takeEvery, take } from 'redux-saga/effects'
import { posts } from '../constants/posts'
import { post } from '../constants/post'

import { postsActions } from '../actions/posts'
import { postActions } from '../actions/post'

import { fetchPosts, fetchPost, getPostComments, addPostAction } from '../api'


function* addPost(action) {
    try {
        const data = yield call(addPostAction, action.payload)
        yield put(postsActions.addPost(data))
    } catch (e) {
        yield put(postsActions.submitFormError('Error in submitting form.'))
    }
}

function* handlePosts() { // for post list
    try {
        const data = yield call(fetchPosts)
        console.log('DATA IN SAGA', data)
        yield put(postsActions.fetchPostsSuccess(data))
    } catch (e) {
        console.log('error obj')
        yield put(postsActions.postsThrowError(e.message))
    }
}

function* handlePost(action) { // for single post w/ comments
    try {
        const data = yield call(fetchPost, action.payload)
        console.log('data inside handle post ', data)
        yield put(postActions.fetchPostSuccess(data))
    } catch (e) {
        yield put(postActions.postThrowError(e.message))
    }
}

function* fetchPostComments(action) {
    try {
        const comments = yield call(getPostComments, action.payload)
        const slicedComments = comments.slice(0, 10)
        yield put(postActions.fetchPostCommentSuccess(slicedComments))
    } catch (e) {
        yield put(postActions.fetchPostCommentError(e.message))
    }
}

export function* watchAll() {
    yield all([
        yield takeLatest(posts.GET, handlePosts),
        yield takeLatest(posts.SUBMIT, addPost),
        yield takeLatest(post.GET, handlePost),
        yield takeLatest(post.GET_POST_COMMENTS, fetchPostComments)
    ])
}

