export const posts = {
    GET: 'posts/GET',
    SUCCESS: 'posts/SUCCESS',
    ERROR: 'posts/ERROR',
    ADD: 'posts/ADD',
    SUBMIT: 'posts/SUBMIT',
    SUBMIT_ERROR: 'posts/SUBMIT_ERROR'
}