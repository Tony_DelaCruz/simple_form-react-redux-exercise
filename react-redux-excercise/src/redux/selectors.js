import { createSelector } from 'reselect'

//posts
const posts = state => state.posts.posts
const error = state => state.posts.error
const formError = state => state.posts.form_error
const postsLoading = state => state.posts.loading


//post
const post = state => state.post
const postError = state => state.post.error
const comments = state => state.post.comments
const postLoading = state => state.post.loading


//post
export const getPostFromState = createSelector([post], post => post)
export const getPostCommentsFromState = createSelector([comments], comments => comments)
export const getPostLoadingFromState = createSelector([postLoading], loading => loading)
export const getPostErrorFromState = createSelector([postError], error => error)

//posts
export const getPostsFromState = createSelector([posts], posts => posts)
export const getErrorFromState = createSelector([error], error => error)
export const getFormErrorFromState = createSelector([formError], error => error)
export const getPostsLoadingFromState = createSelector([postsLoading], loading => loading)