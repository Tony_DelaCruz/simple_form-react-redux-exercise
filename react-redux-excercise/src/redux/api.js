export const fetchPosts = () => {
    const url = 'https://jsonplaceholder.typicode.com/posts'
    return fetch(url)
        .then(data => data.json())
        .then(data => data)

}

export const fetchPost = id => {
    const url = `https://jsonplaceholder.typicode.com/posts/${id}`
    return fetch(url)
        .then(data => data.json())
        .then(data => data)
}

export const getPostComments = id => {
    const url = `https://jsonplaceholder.typicode.com/posts/${id}/comments`

    return fetch(url)
        .then(data => data.json())
        .then(data => data)
}

export const addPostAction = post => {
    const url = 'https://jsonplaceholder.typicode.com/posts'
    console.log('post params', post)
    const response = fetch(url, {
        method: 'POST',
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(post)
    })
        .then(data => data.json())
        .then(data => data)


    return response
}