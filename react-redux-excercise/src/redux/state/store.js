import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import { postReducer } from '../reducers/postsReducer'
import { rootReducer } from '../reducers'
import { watchAll } from '../sagas/postSaga'
import createSagaMiddleWare from 'redux-saga'

const logger = createLogger()
const sagaMiddleWare = createSagaMiddleWare()
export const store = createStore(
    rootReducer,
    applyMiddleware(logger, sagaMiddleWare)
)

sagaMiddleWare.run(watchAll)

console.log('store', store.getState())