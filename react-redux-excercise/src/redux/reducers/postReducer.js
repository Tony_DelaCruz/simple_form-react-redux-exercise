import { post } from '../constants/post'

const initialState = {
    post: {},
    comments: [],
    comment_error: '',
    loading: false,
    comments_loading: false,
    error: ''
}
//DONE // FIXME: Determining if comments and post details are being loaded should not be determine with only one variable. They are independent
export const postReducer = (state = initialState, action) => {
    switch (action.type) {
        case post.GET:
            return {
                ...state,
                loading: !state.loading,
            }
        case post.SUCCESS:
            return {
                ...state,
                post: { ...action.payload },
                loading: !state.loading
            }
        case post.ERROR:
            return {
                ...state,
                loading: !state.loading,
                error: action.payload
            }
        case post.GET_POST_COMMENTS:
            return {
                ...state,
                comments_loading: !state.comments_loading
            }
        case post.GET_POST_COMMENTS_SUCCESS:
            return {
                ...state,
                comments: [...action.payload],
                comments_loading: !state.comments_loading
            }
        case post.GET_POST_COMMENTS_ERROR:
            return {
                ...state,
                comments_loading: !state.loading,
                comment_error: action.payload
            }
        default:
            return state
    }
}
