
import { posts } from '../constants/posts'

const initialState = {
    posts: [],
    loading: false,
    form_submitting: false,
    form_error: '',
    error: ''
}

export const postsReducer = (state = initialState, action) => {
    switch (action.type) {
        case posts.SUBMIT:
            return {
                ...state,
                form_submitting: !state.form_submitting,
            }
        case posts.SUBMIT_ERROR:
            return {
                ...state,
                form_submitting: !state.form_submitting,
                form_error: action.payload
            }
        case posts.ADD:
            return {
                ...state,
                form_submitting: !state.form_submitting,
                posts: [...state.posts, action.payload]
            }
        case posts.GET:
            return {
                ...state,
                loading: !state.loading
            }
        case posts.SUCCESS:
            return {
                ...state,
                posts: [...state.posts, ...action.payload],
                loading: !state.loading
            }
        case posts.ERROR:
            return {
                ...state,
                loading: !state.loading,
                error: action.payload
            }
        default:
            return state
    }
}