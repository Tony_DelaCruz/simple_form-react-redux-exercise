import React, { Component } from 'react';
import './App.scss';

class App extends Component {

    componentDidMount = () => {
        this.props.history.push({
            pathname: '/posts',
            search: '?pages=1'
        })
    }

    render() {
        return <div />
    }
}

export default App;
