export const postRoutes = {
    HOME: '/',
    LIST: '/posts/',
    DETAILS: '/posts/details/:id',
    CREATE: '/posts/create'
}