import React, { Component } from 'react'
import { Button } from '@material-ui/core'
import '../App.scss'



export default class Pagination extends Component {
    renderPageNumbers = () => {
        const { currentPage, postPerPage, posts } = this.props
        const pageNumbers = []

        for (let i = 1; i <= Math.ceil(posts.length / postPerPage); i++) {
            pageNumbers.push(i)
        }

        const pages = pageNumbers.map(num =>
            <button
                className={`btn btn-primary ${num === currentPage ? 'active' : ''}`}
                value={num}
                key={num}
                onClick={this.props.selectPage}
            >
                {num}
            </button>
        )

        return (
            <>
                <Button
                    onClick={this.props.prevPage}
                    variant='contained'
                    color='primary'
                    disabled={this.props.currentPage <= 1}
                >
                    PREV
                    </Button>
                {pages}
                <Button
                    onClick={this.props.nextPage}
                    variant='contained'
                    color='primary'
                    disabled={this.props.currentPage >= pageNumbers.length}
                >
                    NEXT
                </Button>
            </>
        )
    }
    render() {
        return (
            <>{this.renderPageNumbers()}</>
        )
    }
}
