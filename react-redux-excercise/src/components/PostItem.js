import React, { Component } from 'react'
import { Card, Typography, Button } from '@material-ui/core'
import { withRouter } from 'react-router-dom'
import '../App.scss'

class PostItem extends Component {
    render() {
        return (
            <Card
                className='c-PostList item'
                key={this.props.item.id}
            >
                <Typography gutterBottom variant='title'>
                    {this.props.item.title}
                    <Button
                        variant='contained'
                        color='secondary'
                        onClick={() => this.props.history.push(`/posts/details/${this.props.item.id}`)}
                    >
                        View
                    </Button>
                </Typography>
            </Card>
        )
    }
}

export default withRouter(PostItem)