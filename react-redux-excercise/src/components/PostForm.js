import React, { Component } from 'react'
import { Formik as FForm, Form, Field, ErrorMessage } from 'formik'
import { connect } from 'react-redux'
import { Button } from '@material-ui/core'
import { postsActions } from '../redux/actions/posts'
import { getPostsFromState, getFormErrorFromState, getPostsLoadingFromState } from '../redux/selectors'
import * as yup from 'yup'

const formSchema = yup.object().shape({
    title: yup.string()
        .required('Title is required!'),
    body: yup.string()
        .required('Body is required!')
})

const mapStateToProps = state => ({
    posts: getPostsFromState(state),
    form_submitting: state.posts.form_submitting,
    form_error: getFormErrorFromState(state)
})

class PostForm extends Component {
    onSubmit = values => {
        const postValues = {
            ...values,
            userId: 1
        }
        this.props.dispatch(postsActions.submitForm(postValues))
    }

    componentDidUpdate = (prevProps, prevState) => {
        !this.props.form_submitting && !this.props.form_error && this.props.history.push('/')
    }


    //DONE // FIXME: This is not how the status of submission should be checked.

    render() {
        console.log('FORM LOADING', this.props.form_submitting)
        console.log('SUBMITTED', this.state)
        console.log('FORM ERROR', this.props.form_error)
        return (
            <div className='c-Form container'>
                <Button className='c-Form redirect' variant='outlined' onClick={() => this.props.history.push('/')}>Back to List</Button>
                <FForm
                    initialValues={{
                        title: '',
                        body: ''
                    }}
                    validationSchema={formSchema}
                    onSubmit={this.onSubmit}
                >
                    {({ errors, touched }) => (
                        <Form>
                            <div className='form-input'>
                                <Field placeholder='Title' variant='outlined' name='title' />
                                <ErrorMessage name="title" render={err => <div className='error'>{err}</div>} />
                                <Field placeholder='Body' variant='outlined' name='body' />
                                <ErrorMessage name="body" render={err => <div className='error'>{err}</div>} />
                                <Button disabled={this.props.form_submitting} variant='contained' color='primary' type='submit'>
                                    {this.props.form_submitting ? 'Submitting...' : 'Submit'}
                                </Button>
                                {this.props.form_error && <div className='error'>{this.props.form_error}</div>}
                            </div>
                        </Form>
                    )}
                </FForm>
            </div>
        )
    }
}

export default connect(
    mapStateToProps
)(PostForm)
