import React, { Component } from 'react'
import { connect } from 'react-redux'
import { postRoutes } from '../routes'
import { postsActions } from '../redux/actions/posts'
import { Card, Grid, Button, Typography } from '@material-ui/core'
import { getPostsFromState, getErrorFromState, getPostsLoadingFromState } from '../redux/selectors'
import PostItem from './PostItem'
import Pagination from './Pagination';

const mapStateToProps = state => ({
    posts: getPostsFromState(state),
    error: getErrorFromState(state),
    loading: getPostsLoadingFromState(state)
})

class PostList extends Component {

    state = {
        postsPerPage: 10,
        currentPage: ''
    }

    componentDidMount = () => {
        const queryParams = this.props.location.search.split('=')

        const queryParamValue = Number(queryParams[queryParams.length - 1])

        this.setState({ currentPage: queryParamValue })

        if (this.props.posts.length === 0)
            return this.props.dispatch(postsActions.fetchPosts())
        return
    }

    createItem = () => this.props.history.push(postRoutes.CREATE)
    viewItem = id => this.props.history.push(`/posts/details/${id}`)
    PageNumToggle = e => this.setState({ currentPage: Number(e.target.value) }, () => this.props.history.push({ search: `?pages=${this.state.currentPage}` }))
    nextPage = () => this.setState({ currentPage: this.state.currentPage + 1 }, () => this.props.history.push({ search: `?pages=${this.state.currentPage}` }))
    prevPage = () => this.setState({ currentPage: this.state.currentPage - 1 }, () => this.props.history.push({ search: `?pages=${this.state.currentPage}` }))

    renderPosts() {
        const { currentPage, postsPerPage } = this.state
        const { error, loading } = this.props
        const lastTodo = currentPage * postsPerPage
        const firstTodo = lastTodo - postsPerPage
        const postsRenderedPerPage = this.props.posts.slice(firstTodo, lastTodo)

        if (error)
            return <Typography variant='h3'>{this.props.error}</Typography>
        // DONE // Status of the API request should be checked instead of the number of posts when determining if it is still fetching.
        if (loading)
            return <Typography variant='h3'>Fetching</Typography>

        return postsRenderedPerPage.map(item =>
            <PostItem
                item={item}
            />
        )
    }
    render() {
        //DONE  // FIXME: Avoid using arrow functions inside the render function. See https://reactjs.org/docs/faq-functions.html
        //DONE  // FIXME: Paginator should be a separate component
        //DONE  // FIXME: Post list and post items should be in their own components
        return (
            <div className='c-PostList container' spacing={24}>
                <Grid className='c-PostList header' container spacing={16}>
                    <Grid item><Typography variant='h2'>Post List...</Typography></Grid>
                    <Grid item><Button variant='outlined' color='primary' onClick={() => this.createItem()}>ADD POST</Button></Grid>
                </Grid>
                {this.renderPosts()}
                <div className='pagination'>
                    <Pagination
                        posts={this.props.posts}
                        currentPage={this.state.currentPage}
                        postPerPage={this.state.postsPerPage}
                        nextPage={this.nextPage}
                        prevPage={this.prevPage}
                        selectPage={this.PageNumToggle}
                    />
                </div>
            </div >
        )
    }
}

export default connect(
    mapStateToProps
)(PostList)
