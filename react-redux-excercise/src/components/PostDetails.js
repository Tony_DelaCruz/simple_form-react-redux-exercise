import React, { Component } from 'react'
import { connect } from 'react-redux'
import { postActions } from '../redux/actions/post'
import '../App.scss'
import { Typography, Card, CardHeader, CardContent, Button } from '@material-ui/core'
import { getPostFromState, getPostCommentsFromState, getPostLoadingFromState, getPostErrorFromState } from '../redux/selectors'

const mapStateToProps = state => ({
    post: getPostFromState(state),
    comments: getPostCommentsFromState(state),
    comments_loading: state.post.comments_loading,
    loading: getPostLoadingFromState(state),
    error: getPostErrorFromState(state)
})

class PostDetails extends Component {
    state = {
        post: ''
    }
    componentDidMount = () => {
        this.props.dispatch(postActions.fetchSinglePost(this.props.match.params.id))
        this.props.dispatch(postActions.fetchPostComments(this.props.match.params.id))
    }

    componentDidUpdate(prevProps, prevState) {
        if (Object.keys(prevProps.post.post).length > 0) {
            console.log('PREV PROPS', prevProps)
        }
    }

    backToList = () => {
        this.props.history.push('/')
    }

    renderComments = () => {
        const { comments_loading, comments, error } = this.props
        if (error)
            return <Typography variant='h4'>{error}</Typography>

        return comments_loading ? 'Fetching Commnets...' : comments.map(item =>
            <Card key={item.name} className='c-Details card'>
                <CardHeader
                    title={item.name}
                    subheader={item.email}
                />
                <CardContent>
                    <Typography paragraph>
                        {item.body}
                    </Typography>
                </CardContent>
            </Card>
        )
    }

    renderHeader = () => {
        const { loading, post: { post }, error } = this.props
        if (error)
            return <Typography variant='h4'>{error}</Typography>
        return loading ? 'Fetching...' :
            <>
                <div><Typography variant='h2'>{post.title}</Typography></div><br />
                <div><Typography variant='headline'>{post.body}</Typography></div>
            </>
    }

    render() {
        return (
            <div className='c-Details container'>
                <Button variant='outlined' onClick={() => this.backToList()}>Back to List</Button>
                <div>
                    {this.renderHeader()}
                    <br />
                    <Typography variant='h6'>COMMENTS:</Typography>
                    {this.renderComments()}
                </div>
            </div >
        )
    }
}

export default connect(
    mapStateToProps
)(PostDetails)
