import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux'
import { store } from './redux/state/store';
import PostList from './components/PostList'
import PostDetails from './components/PostDetails'
import PostForm from './components/PostForm'
import { postRoutes } from './routes'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'


ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Switch>
                <div className='App'>
                    <Route exact path={postRoutes.HOME} component={App} />
                    <Route exact path={postRoutes.LIST} component={PostList} />
                    <Route path={postRoutes.DETAILS} component={PostDetails} />
                    <Route path={postRoutes.CREATE} component={PostForm} />
                </div>
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root')
);

